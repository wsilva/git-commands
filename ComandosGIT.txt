﻿/**
*
*
* @author Pedro Ribeiro - @pcelta
*
*
*/




<Comandos GIT>


[ Commits ]

git add -A
// Adiciona todas as alterações, inclusive arquivos novos,  para commit


git commit -m ‘commit descritpion’
// Consolida as alterações adicionadas ao commit, com uma descrição


git commit -a -m "commit description"
// Adiciona e consolidada as alterações, mas não adiciona novos arquivos




[ Branchs ]


git branch name_branch
// cria um branch novo apartir do branch atual


git branch -m name_old_branch name_new_branch
// renomear um branch


git branch nomebranch -d
// Deleta um branch localmente atualizado


git branch nomebranch -D
// Deleta branch localmente independente da situação dele




git push origin :nome_do_branch
// Excluir branch remoto


git branch -a
// Lista branchs remotos


git push --force origin branchremoto
// realiza o push e sobrescreve o branch do servidor pelo seu branch local


git remote prune origin
// remove localmente todos os branches que já foram removidos remotamente - espécie de sync


git push origin branch_local:branch_remoto
// cria um branch remoto baseado em um branch local


git push -u origin branch_local:branch_remoto
// cria um branch remoto baseado em um branch local, mantendo os branches vinculados 


git branch --set-upstream meu_branch origin/meu_branch
// vincula um branch local a um branch remoto (caso ainda não exista esta vinculação)


[ Log ]


git log - p nome_arquivo
// mostra o que foi alterado em cada commit em um arquivo


git log --author=Name Author
// mostra apenas commits e um autor específico



git blame nome_arquivo
// mostra quem foi o autor de cada linha de um arquivo


git reset --hard HEAD^
// desfaz as alterações consolidadas no último commit


git reset --hard SHA1DOCOMMIT
// desfaz as alterações consolidadas depois do commit específicado



[ Whatchanged ]


git whatchanged
// mostra quais arquivos foram alterados em cada commit


git whatchanged --author=Name Author
// mostra quais arquivos foram alterados em cada commit de um autor específico




[ Checkout ]

git checkout -f
// desfaz as alterações não consolidadas no branch atual


git checkout nome_arquivo
// desfaz as alterações não consolidadas em um arquivo


git checkout -b nomeDoBranch origin/nomeDoBranch
// limpa um branch e realiza o pull do servidor para um único branch




[ Ignore ]

git update-index --assume-unchanged <filename(s)>
//Para ignorar arquivos já existentes em seu projeto


git update-index --no-assume-unchanged <filename(s)>
//Para "designorar" arquivos ignorados em seu projeto



git ls-files -v | grep "h "   
####  atenção: esse espaçozinho depois do 'h' é obrigatório ####
//Para listar arquivos já ignorados em seu projeto






[ Tags ]

git push --tags
// realiza o push e envia as tags criadas para o remote


git tag -d tag_name
// remove uma tag localmente


git push origin :refs/tags/tag_name
// remove tag já enviada ao servidor - 12345 é a tag


git push origin tag_name
//envia uma tag criada localmente para o remote



[ Revert ]


git checkout SHA1^ -- <filename>
//Para reverter um arquivo para uma determinada versão


git revert SHA1
//Para reverter para um determinado commit criando um novo commit



[ Reset ]

git reset --hard SHA1
//Para reverter para um determinado commit


git reset --hard HEAD^
//Para reverter o último commit



[ Stash ]

git stash
// move as alterações não adicionadas ao commit para memoria tempoária e limpa o branch das alterações.
// comando deve ser usada quando precisarmos mudar de branch sem commitar as mudanças atuais.


git stash list
// mostra os stashs criados, exemplo: 
/*
 *   stash@{0}: WIP on branch_name
 *   stash@{1}: WIP on branch_name
**/


git stash apply stash@{1}
// retorna as alterações do stash 1


git stash pop
// retorna as alterações do ultimo stash criado e o remove da pilha.


